#include <SFML/Graphics.hpp>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include <limits>
#include <thread>
#include <mutex>

#define SCR_SIZE 640
#define PI 3.141592653589793238462643

using namespace std;

string filename = "C266";
float K = 0.01;//accuracy
int lb_num = 4;//main local search threads

enum GST {//global search type
	GR, CL, PT
};
//GR - greedy, CL - clustered, PT - petal
constexpr GST GLOBALSEARCHTYPE = GST::GR;
constexpr bool use2opt = true;
constexpr bool useCross = true;
constexpr bool useSwap = true;
constexpr bool useInsert = true;
constexpr bool useDrop = true;

bool checkTime = true;//for checking answers correctness from .sol files

struct CUST {
	int index;
	float x;
	float y;
	int demand;
	float readyTime;
	float dueTime;
	float serviceTime;
	sf::Color color = sf::Color::White;

	void Print() {
		cout << setw(4) << index << " " << setw(3) << x << " " << setw(3) << y << " " << setw(4) << demand << " " << setw(6) << readyTime << " " << setw(6) << dueTime << " " << setw(6) << serviceTime << endl;
	}
};

struct SOLUTION {
	vector<vector<pair<int, float>>> routes;
	float cost;

	void Print() {
		cout << filename << endl << "Solution cost: " << cost << endl;
		for (int m = 0; m < routes.size(); ++m) {
			//cout << m << "!!!" << endl;
			for (int i = 0; i < routes[m].size(); ++i) {
				cout << routes[m][i].first << " " << routes[m][i].second << " ";
			}
			cout << endl;
		}
	}
};

SOLUTION GenerateFeasibleSolution();

////////////////////////////////////////////////////////////////////////////////////////////////////////

mutex shutdown_mutex;
bool shutdown = false;
mutex pause_mutex;
bool pause = false;

float GUIScale;

int num_cars;
int capacity;

vector<CUST> customers;

vector<vector<float>> customers_distance;

//mutex global_penalty_mutex; //it sounds stupid but i remove this mutex in case of random search implementation via multithreading lol
vector<vector<float>> global_penalty;

vector<sf::Color> cars_color;

mutex total_best_solution_mutex;
SOLUTION total_best_solution;

////////////////////////////////////////////////////////////////////////////////////////////////////////

float get_customers_distance(int c1, int c2) {
	return sqrt(pow(customers[c1].x - customers[c2].x, 2) + pow(customers[c1].y - customers[c2].y, 2));
}

void Gladden(SOLUTION & s) {
	cout << "New best: " << setprecision(std::numeric_limits<long double>::digits10 + 1) << s.cost << " " << s.routes.size() << endl;
	for (int m = 0; m < s.routes.size(); ++m) {
		for (int i = 1; i < s.routes[m].size() - 1; ++i) {
			customers[s.routes[m][i].first].color = cars_color[m];
		}
	}
}

void Penalty(SOLUTION sol) {
	/*float n = 0;
	for (int m = 0; m < sol.routes.size(); n += sol.routes[m].size() - 1, ++m);
	float l = 0.001 * sol.cost / n;*/
	for (int m = 0; m < sol.routes.size(); ++m) {
		for (int i = 1; i < sol.routes[m].size(); ++i) {
			global_penalty[sol.routes[m][i - 1].first][sol.routes[m][i].first] += K;//works better in global search
		}
	}
}

pair<bool, float> check_solution(SOLUTION & sol) {
	bool valid = true;
	sol.cost = 0;
	bool printError = true;

	vector<bool> customs(customers.size(), false);

	for (int m = 0; m < sol.routes.size(); ++m) {
		int car_capacity = 0;
		for (int i = 0; i < sol.routes[m].size(); ++i) {
			pair<int, float> p = sol.routes[m][i];
			customs[p.first] = true;
			if (p.second < customers[p.first].readyTime || p.second > customers[p.first].dueTime) {
				valid = false;
				if (printError) cout << "out of work time" << endl;
				return { valid, sol.cost };
			}
		}
		for (int i = 1; i < sol.routes[m].size(); ++i) {
			if (sol.routes[m][i - 1].second + customers[sol.routes[m][i - 1].first].serviceTime + customers_distance[sol.routes[m][i - 1].first][sol.routes[m][i].first] <= sol.routes[m][i].second) {
				sol.cost += customers_distance[sol.routes[m][i - 1].first][sol.routes[m][i].first];
				car_capacity += customers[sol.routes[m][i].first].demand;
			} else {
				valid = false;
				if (printError) {
					cout << "wrong time counting" << endl;
					cout << sol.routes[m][i - 1].first << " " << sol.routes[m][i].first << endl;
					cout << sol.routes[m][i - 1].second + customers[sol.routes[m][i - 1].first].serviceTime + customers_distance[sol.routes[m][i - 1].first][sol.routes[m][i].first] << endl;
					cout << sol.routes[m][i].second << endl;
					for (int j = 1; j <= i; ++j) {
						cout << sol.routes[m][j - 1].first << " " << sol.routes[m][j].first << " " << customers_distance[sol.routes[m][j - 1].first][sol.routes[m][j].first] << endl;
					}
				}
				return { valid, sol.cost };
			}
		}
		if (sol.routes[m][sol.routes[m].size() - 1].first != 0) {
			valid = false;
			if (printError) cout << "car end not in deport" << endl;
			return { valid, sol.cost };
		}
		if (car_capacity > capacity) {
			valid = false;
			if (printError) cout << "car capacity out of posible" << endl;
			return { valid, sol.cost };
		}
	}
	for (int i = 0; i < customs.size(); ++i) {
		if (!customs[i]) {
			valid = false;
			if (printError) cout << "not all customers served" << endl;
			break;
		}
	}

	if (sol.routes.size() > num_cars) valid = false;


	return { valid, sol.cost };
}

void init_data(string filename, bool print) {
	customers.clear();
	float mx = 0, my = 0;

	ifstream file(filename);
	if (file.is_open()) {

		string line;
		for (int i = 0; i < 4; ++i, getline(file, line));
		getline(file, line);
		istringstream iss(line);
		string sub;
		iss >> sub; 
		if (print) cout << sub << endl;
		num_cars = stoi(sub);
		for (int i = 0; i < num_cars; cars_color.push_back(sf::Color(rand() % 255, rand() % 255, rand() % 255)), ++i);
		iss >> sub; 
		if (print) cout << sub << endl;
		capacity = stoi(sub);
		for (int i = 0; i < 4; ++i, getline(file, line));
		while (getline(file, line)) {
			string p;
			istringstream iss(line);
			CUST c;
			iss >> p; c.index = stoi(p);
			iss >> p; c.x = stof(p);
			iss >> p; c.y = stof(p);
			iss >> p; c.demand = stoi(p);
			iss >> p; c.readyTime = stof(p);
			iss >> p; c.dueTime = stof(p);
			iss >> p; c.serviceTime = stof(p);
			if (print) c.Print();
			customers.push_back(c);

			mx = max(mx, c.x);
			my = max(my, c.y);
		}
	}

	GUIScale = (float)SCR_SIZE / max(mx, my);

	customers_distance = vector<vector<float>>(customers.size(), vector<float>(customers.size(), 0));
	for (int i = 0; i < customers.size() - 1; ++i) {
		for (int j = i + 1; j < customers.size(); ++j) {
			float d = get_customers_distance(i, j);
			customers_distance[i][j] = customers_distance[j][i] = d;
		}
	}
	global_penalty = vector<vector<float>>(customers.size(), vector<float>(customers.size(), 0));
}

void OutputBest(SOLUTION & sol, string filename) {
	/*for (int m = 0; m < sol.routes.size(); ++m) {
		if (sol.routes[m].size() <= 2) {
			sol.routes.erase(sol.routes.begin() + m);
		}
	}*/
	ofstream file(filename + ".sol");
	file.clear();
	file.precision(numeric_limits<float>::max_digits10);
	total_best_solution_mutex.lock();
	for (int m = 0; m < sol.routes.size(); ++m) {
		for (int i = 0; i < sol.routes[m].size(); ++i) {
			file << sol.routes[m][i].first << " " << sol.routes[m][i].second << " ";
		}
		file << endl;
	}
	file.close();
	total_best_solution_mutex.unlock();
	cout << "Solution writed to " + filename + ".sol" << endl;
}

void UI() {
	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;

	static sf::RenderWindow window(sf::VideoMode(SCR_SIZE, SCR_SIZE), filename, sf::Style::Titlebar | sf::Style::Close | sf::Style::Resize, settings);
	sf::CircleShape vertShape;
	vertShape.setRadius(5);

	vertShape.setFillColor(sf::Color(136, 136, 140));
	vertShape.setOutlineThickness(1);
	vertShape.setOrigin(vertShape.getRadius() / 2 + 3, vertShape.getRadius() / 2 + 3);

	sf::Text text;
	text.setFillColor(sf::Color::White);
	text.setCharacterSize(16);
	sf::Font font;
	font.loadFromFile("Arial.ttf");
	text.setFont(font);

	while (window.isOpen() && !shutdown) {
		sf::Event event;
		while (window.pollEvent(event)) {
			switch (event.type) {
			case sf::Event::Closed:
				shutdown_mutex.lock();
				shutdown = true;
				shutdown_mutex.unlock();
				window.close();
				break;
			case sf::Event::KeyPressed:
				if (event.key.code == sf::Keyboard::Escape) {
					shutdown_mutex.lock();
					shutdown = true;
					shutdown_mutex.unlock();
					window.close();
				} else if (event.key.code == sf::Keyboard::Space) {
					pause_mutex.lock();
					if (!pause) {
						pause = true;
						cout << "Pause" << endl;
					} else {
						pause = false;
						cout << "Unpause" << endl;
					}
					pause_mutex.unlock();
				} else if (event.key.code == sf::Keyboard::P) {
					total_best_solution_mutex.lock();
					total_best_solution.Print();
					total_best_solution_mutex.unlock();
				} else if (event.key.code == sf::Keyboard::R) {
					pause_mutex.lock();
					for (int i = 0; i < customers.size(); ++i) {
						for (int j = 0; j < customers.size(); ++j) {
							global_penalty[i][j] = 0;
						}
					}
					cout << "Renalty refreshed" << endl;
					pause_mutex.unlock();
				} else if (event.key.code == sf::Keyboard::X) {
					total_best_solution.cost = numeric_limits<float>::max();
				} else if (event.key.code == sf::Keyboard::Up) {
					K *= 10;
					cout << "K = " << K << endl;
				} else if (event.key.code == sf::Keyboard::Down) {
					K /= 10;
					cout << "K = " << K << endl;
				} else if (event.key.code == sf::Keyboard::O) {
					OutputBest(total_best_solution, filename);
				}
				break;
			}
		}

		window.clear();
		int tx, ty;
		float angle;

		SOLUTION sol;
		total_best_solution_mutex.lock();
		sol = total_best_solution;
		total_best_solution_mutex.unlock();

		for (int m = 0; m < sol.routes.size(); ++m) {
			for (auto i = 0; i != sol.routes[m].size() - 1; ++i) {
				int tx1 = customers[sol.routes[m][i].first].x * GUIScale;
				int ty1 = customers[sol.routes[m][i].first].y * GUIScale;
				int tx2 = customers[sol.routes[m][i + 1].first].x * GUIScale;
				int ty2 = customers[sol.routes[m][i + 1].first].y * GUIScale;

				float dist = sqrt(pow(tx1 - tx2, 2) + pow(ty1 - ty2, 2));
				sf::RectangleShape rect(sf::Vector2f(2, dist));
				angle = atan2(ty1 - ty2, tx1 - tx2) * 180 / PI + 90;
				rect.rotate(angle);
				rect.setPosition(tx1, ty1);
				rect.setFillColor(cars_color[m]);
				rect.setOrigin(2, 0);
				window.draw(rect);
			}
		}

		for (int i = 0; i < customers.size(); ++i) {
			angle = ((float)360 / (float)customers.size()) * i;
			tx = customers[i].x * GUIScale;
			ty = customers[i].y * GUIScale;
			vertShape.setPosition(tx, ty);
			vertShape.setFillColor(customers[i].color);
			window.draw(vertShape);

			if (false) {
				stringstream ss;
				ss << i;
				text.setString(ss.str().c_str());
				text.setPosition(vertShape.getPosition());
				window.draw(text);
			}
		}

		window.display();
	}
}

bool CarCanReach(int curCust, float curTime, int curCapacity, int destinationCust) {
	float readyTime = curTime + customers_distance[curCust][destinationCust];
	if (readyTime > customers[destinationCust].dueTime) return false;
	if (curCapacity - customers[destinationCust].demand < 0) return false;
	if (max(readyTime, customers[destinationCust].readyTime) + customers[destinationCust].serviceTime + customers_distance[destinationCust][0] > customers[0].dueTime) return false;

	return true;
}

void CompressRoute(vector<vector<pair<int, float>>>& r, int m) {
	for (int i = 1; i < r[m].size(); ++i) {
		r[m][i].second = max(r[m][i - 1].second + customers[r[m][i - 1].first].serviceTime + customers_distance[r[m][i - 1].first][r[m][i].first], customers[r[m][i].first].readyTime);
	}
}

SOLUTION GenerateFeasibleSolution() {
	while (true) {
		bool solution_broken = false;

		SOLUTION solution;

		vector<int> unservedCustomers;
		for (int i = 1; i < customers.size(); ++i) {
			unservedCustomers.push_back(i);
		}

		int cur_car_index = -1;
		int cur_car_capacity = capacity;
		int cur_car_position = 0;
		float cur_car_time = 0;

		bool need_new_car = true;

		float cx = customers[0].x;
		float cy = customers[0].y;

		while (!unservedCustomers.empty()) {
			if (need_new_car) {
				++cur_car_index;
				if (cur_car_index >= num_cars) {
					solution_broken = true;
					break;
				}
				cur_car_capacity = capacity;
				cur_car_position = 0;
				cur_car_time = 0;

				cx = customers[0].x;
				cy = customers[0].y;

				solution.routes.push_back(vector<pair<int, float>>());
				solution.routes[cur_car_index].push_back(pair<int, float>(0, 0));
				need_new_car = false;
			}

			int best_candidate = 0;
			int best_candidate_index = 0;
			float best_candidate_cost = numeric_limits<float>::max();

			for (int i = 0; i < unservedCustomers.size(); ++i) {
				int c = unservedCustomers[i];
				if (CarCanReach(cur_car_position, cur_car_time, cur_car_capacity, c)) {
					float cost;
					if (GLOBALSEARCHTYPE == GST::GR) {
						cost = customers_distance[cur_car_position][c] + max(customers[c].readyTime - cur_car_time - customers_distance[cur_car_position][c], (float)0) + global_penalty[cur_car_position][c];
					} else if (GLOBALSEARCHTYPE == GST::CL) {
						cost = sqrt(pow(cx - customers[c].x, 2) + pow(cy - customers[c].y, 2)) + max(customers[c].readyTime - cur_car_time - customers_distance[cur_car_position][c], (float)0) + global_penalty[cur_car_position][c];
					} else if (GLOBALSEARCHTYPE == GST::PT) {
						cost = sqrt(pow(cx - customers[c].x, 2) + pow(cy - customers[c].y, 2)) + max(customers[c].readyTime - cur_car_time - customers_distance[cur_car_position][c], (float)0) + global_penalty[cur_car_position][c];
					}
					if (cost < best_candidate_cost) {
						best_candidate = c;
						best_candidate_index = i;
						best_candidate_cost = cost;
					}
				}
			}

			if (best_candidate == 0) {
				solution.routes[cur_car_index].push_back(pair<int, float>(0, cur_car_time + customers_distance[cur_car_position][0]));
				need_new_car = true;
			} else {
				cur_car_time = max(cur_car_time + customers_distance[cur_car_position][best_candidate], customers[best_candidate].readyTime);
				cur_car_position = best_candidate;
				solution.routes[cur_car_index].push_back(pair<int, float>(cur_car_position, cur_car_time));
				cur_car_time += customers[best_candidate].serviceTime;
				cur_car_capacity -= customers[best_candidate].demand;

				if (GLOBALSEARCHTYPE == GST::CL) {
					cx = cy = 0;
					for (int i = 0; i < solution.routes[cur_car_index].size(); ++i) {
						cx += customers[solution.routes[cur_car_index][i].first].x;
						cy += customers[solution.routes[cur_car_index][i].first].y;
					}
					cx /= solution.routes[cur_car_index].size();
					cy /= solution.routes[cur_car_index].size();
				} else if (GLOBALSEARCHTYPE == GST::PT) {
					cx = customers[1].x + ((customers[solution.routes[cur_car_index].size() - 1].x - customers[0].x) / 1);
					cy = customers[1].y + ((customers[solution.routes[cur_car_index].size() - 1].y - customers[0].y) / 1);
				}

				unservedCustomers.erase(unservedCustomers.begin() + best_candidate_index);

				if (unservedCustomers.empty()) {
					solution.routes[cur_car_index].push_back(pair<int, float>(0, cur_car_time + customers_distance[cur_car_position][0]));
				}
			}
		}

		if (!solution_broken) {
			pair<bool, float> result = check_solution(solution);
			if (result.first) {
				Penalty(solution);
				return solution;
			}
		}
	}
}

void LocalBrute() {
	SOLUTION s = GenerateFeasibleSolution();
	shutdown_mutex.lock();
	while (!shutdown) {
		shutdown_mutex.unlock();
		pause_mutex.lock();
		if (pause) {
			pause_mutex.unlock();
			shutdown_mutex.lock();
			continue;
		}
		pause_mutex.unlock();

		SOLUTION s1;
		int c;
		pair<bool, float> p;
		pair<int, float> node;

		bool improved = false;

		if (use2opt) {
			for (int m = 0; m < s.routes.size(); ++m) {
				for (int i = 1; i < s.routes[m].size() - 2; ++i) {
					for (int j = i + 1; j < s.routes[m].size(); ++j) {
						//2-opt :/
						s1 = s;
						vector<pair<int, float>> r1 = s1.routes[m];
						int d = j - i + 1;
						for (int k = 0; k < d; ++k) {
							s1.routes[m][i + k] = r1[j - k];
						}
						CompressRoute(s1.routes, m);
						p = check_solution(s1);
						if (p.first) {
							if (s1.cost < s.cost) {
								s = s1;
								improved = true;
								total_best_solution_mutex.lock();
								if (s.cost < total_best_solution.cost) {
									total_best_solution = s;
									Gladden(total_best_solution);
									cout << "2-opt" << endl;
								}
								total_best_solution_mutex.unlock();
							}
						}
					}
				}
			}
		}
		if (useCross) {
			for (int m = 0; m < s.routes.size(); ++m) {
				for (int i = 1; i < s.routes[m].size() - 1; ++i) {
					for (int m1 = m + 1; m1 < s.routes.size(); ++m1) {
						for (int i1 = 1; i1 < s.routes[m1].size() - 1; ++i1) {
							//cross
							s1 = s;
							vector<pair<int, float>> r1, r2;
							for (int j = 0; j < i; r1.push_back(s1.routes[m][j++]));
							for (int j = i1; j < s1.routes[m1].size(); r1.push_back(s1.routes[m1][j++]));
							for (int j = 0; j < i1; r2.push_back(s1.routes[m1][j++]));
							for (int j = i; j < s1.routes[m].size(); r2.push_back(s1.routes[m][j++]));
							s1.routes[m] = r1;
							s1.routes[m1] = r2;

							CompressRoute(s1.routes, m);
							CompressRoute(s1.routes, m1);
							p = check_solution(s1);
							if (p.first) {
								if (s1.cost < s.cost) {
									s = s1;
									improved = true;
									total_best_solution_mutex.lock();
									if (s.cost < total_best_solution.cost) {
										total_best_solution = s;
										Gladden(total_best_solution);
										cout << "cross" << endl;
									}
									total_best_solution_mutex.unlock();
								}
							}
						}
					}
				}
			}
		}
		if (useSwap) {
			for (int m = 0; m < s.routes.size(); ++m) {
				for (int i = 1; i < s.routes[m].size() - 1; ++i) {
					for (int m1 = m; m1 < s.routes.size(); ++m1) {
						for (int i1 = 1; i1 < s.routes[m1].size() - 1; ++i1) {
							//swap
							s1 = s;
							c = s1.routes[m][i].first;
							s1.routes[m][i].first = s1.routes[m1][i1].first;
							s1.routes[m1][i1].first = c;
							CompressRoute(s1.routes, m);
							CompressRoute(s1.routes, m1);
							p = check_solution(s1);
							if (p.first) {
								if (s1.cost < s.cost) {
									s = s1;
									improved = true;
									total_best_solution_mutex.lock();
									if (s.cost < total_best_solution.cost) {
										total_best_solution = s;
										Gladden(total_best_solution);
										cout << "swap" << endl;
									}
									total_best_solution_mutex.unlock();
								}
							}
						}
					}
				}
			}
		}
		if (useInsert) {
			for (int m = 0; m < s.routes.size(); ++m) {
				for (int i = 1; i < s.routes[m].size() - 1; ++i) {
					for (int m1 = 0; m1 < s.routes.size(); ++m1) {
						for (int i1 = 1; i1 < s.routes[m1].size(); ++i1) {
							//insert
							s1 = s;
							node = s1.routes[m][i];
							s1.routes[m].erase(s1.routes[m].begin() + i);
							s1.routes[m1].insert(s1.routes[m1].begin() + i1, node);
							CompressRoute(s1.routes, m);
							CompressRoute(s1.routes, m1);
							p = check_solution(s1);
							if (p.first) {
								if (s1.cost < s.cost) {
									s = s1;
									improved = true;
									total_best_solution_mutex.lock();
									if (s.cost < total_best_solution.cost) {
										total_best_solution = s;
										Gladden(total_best_solution);
										cout << "insert" << endl;
									}
									total_best_solution_mutex.unlock();
								}
							}
						}
					}
				}
			}
		}
		if (useDrop) {
			for (int m = 0; m < s.routes.size(); ++m) {
				for (int i = 1; i < s.routes[m].size() - 1; ++i) {

					//drop to the new route
					s1 = s;
					node = s1.routes[m][i];
					s1.routes[m].erase(s1.routes[m].begin() + i);
					s1.routes.push_back(vector<pair<int, float>>(3));
					s1.routes[s1.routes.size() - 1][0] = s1.routes[s1.routes.size() - 1][2] = pair<int, float>(0, 0);
					s1.routes[s1.routes.size() - 1][1] = node;
					CompressRoute(s1.routes, m);
					CompressRoute(s1.routes, s1.routes.size() - 1);
					p = check_solution(s1);
					if (p.first) {
						if (s1.cost < s.cost) {
							s = s1;
							improved = true;
							total_best_solution_mutex.lock();
							if (s.cost < total_best_solution.cost) {
								total_best_solution = s;
								Gladden(total_best_solution);
								cout << "drop" << endl;
							}
							total_best_solution_mutex.unlock();
						}
					}
				}
			}
		}

		if (!improved) {
			s = GenerateFeasibleSolution();
		}

		shutdown_mutex.lock();
	}
	shutdown_mutex.unlock();
}

void CheckSolutions() {
	vector<string> sols = { "C108" , "C203", "C249", "C266", "R146", "R168", "R202", "R1103", "R1104", "R1105", "R1107", "R11010", "RC105", "RC148", "RC207" };
	for (int i = 0; i < sols.size(); ++i) {
		init_data(sols[i] + ".txt", false);
		ifstream file(sols[i] + ".sol");
		if (file.is_open()) {
			SOLUTION sol;
			string line;
			while (getline(file, line)) {
				sol.routes.push_back(vector<pair<int, float>>());
				istringstream iss(line);
				int cust;
				float time;
				while (iss >> cust >> time) {
					sol.routes[sol.routes.size() - 1].emplace_back(cust, time);
				}
			}
			float cost = 0;
			for (int m = 0; m < sol.routes.size(); ++m) {
				for (int i = 1; i < sol.routes[m].size(); ++i) {
					cost += customers_distance[sol.routes[m][i - 1].first][sol.routes[m][i].first];
				}
			}
			file.close();
			//rewrite float to full format
			/*for (int m = 0; m < sol.routes.size(); ++m) {
				CompressRoute(sol.routes, m);
			}*/
			//OutputBest(sol, sols[i]);
			//sol.Print();
			cout << sols[i] << ": " << cost << " " << (check_solution(sol).first ? "Correct" : "Error") << endl;
		}
		
	}
}

void main() {
	cout.precision(numeric_limits<float>::max_digits10);
	if (checkTime) {
		CheckSolutions();
		cin.get();
	} else {
		init_data(filename + ".txt", true);

		total_best_solution = { {}, numeric_limits<float>::max() };

		thread drawing(UI);

		vector<thread> lb_trds;
		for (int i = 0; i < lb_num; lb_trds.push_back(thread(LocalBrute)), ++i);
		for (int i = 0; i < lb_num; lb_trds[i++].join());

		drawing.join();
	}

	
}
